-- Deploy stream:stream_table to pg
-- requires: appschema
BEGIN;

CREATE TABLE stream_table (
	id	SERIAL	PRIMARY KEY,
	game	varchar(30)	NOT NULL,
	stream_strt	TIMESTAMP NOT NULL,
	stream_end	TIMESTAMP
	);

COMMIT;

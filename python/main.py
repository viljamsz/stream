import uvicorn
import asyncpg
import asyncio

from pathlib import Path
import fastapi_chameleon
from fastapi import FastAPI
from datetime import datetime

BASE_DIR = Path(__file__).resolve().parent
template_folder = str(BASE_DIR / 'templates')
fastapi_chameleon.global_init(template_folder)

app = FastAPI()

@app.get("/streamtab")
@fastapi_chameleon.template('stream_table.pt')
async def stream():
    conn = await asyncpg.connect(user='viljams', database='stream')
    result = await conn.fetch('SELECT * FROM stream_table')
    result_list = [dict(record) for record in result]

    for item in result_list:
        item['stream_strt'] = item['stream_strt'].strftime("%Y, %b %d %H:%M")
        item['stream_end'] = item['stream_end'].strftime("%Y, %b %d %H:%M")
    return {'streams': result_list}